<?php

return [
    'quicktips.wikipedia.adress' => 'aus <a href="https://de.wikipedia.org" target="_blank" rel="noopener">Wikipedia, der freien Enzyklopädie</a>',
    'quicktips.dictcc.adress'    => 'aus <a href="https://www.dict.cc/" target="_blank" rel="noopener">dict.cc</a>',
    'quicktips.tips.title'       => 'Wussten Sie schon?',
];
