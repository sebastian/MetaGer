<?php

return [
    'results.failed'           => 'Leider konnten wir zu Ihrer Sucheingabe keine passenden Ergebnisse finden.',

    'settings.noneSelected'    => 'Achtung: Sie haben in ihren Einstellungen keine Suchmaschine ausgewählt.',

    'engines.noParser'         => 'Beim Abfragen von :engine ist ein Fehler aufgetreten. Bitte benachrichtigen Sie uns unter: <a href="mailto:office@suma-ev.de?subject=Fehlender Parser: :engine">office@suma-ev.de</a>',

    'formdata.cantLoad'        => 'Suma-File konnte nicht gefunden werden',
    'formdata.noSearch'        => 'Achtung: Sie haben keinen Suchbegriff eingegeben. Sie können ihre Suchbegriffe oben eingeben und es erneut versuchen.',
    'formdata.dartEurope'      => 'Hinweis: Sie haben Dart-Europe aktiviert. Die Suche kann deshalb länger dauern und die maximale Suchzeit wurde auf 10 Sekunden hochgesetzt.',
    'formdata.hostBlacklist'   => 'Ergebnisse von folgenden Hosts werden nicht angezeigt: ":host"',

    'formdata.domainBlacklist' => 'Ergebnisse von folgenden Domains werden nicht angezeigt: ":domain"',
    'formdata.stopwords'       => 'Sie machen eine Ausschlusssuche. Ergebnisse mit folgenden Wörtern werden nicht angezeigt: ":stopwords"',
    'formdata.phrase'          => 'Sie führen eine Phrasensuche durch: :phrase',

    'sitesearch.failed'        => 'Sie wollten eine Sitesearch auf :site durchführen. Leider unterstützen die eingestellten Suchmaschinen diese nicht. Sie können <a href=":searchLink">hier</a> die Sitesearch im Web-Fokus durchführen. Es werden ihnen Ergebnisse ohne Sitesearch angezeigt.',
    'sitesearch.success'       => 'Sie führen eine Sitesearch durch. Es werden nur Ergebnisse von der Seite: <a href="http://:site" target="_blank" rel="noopener">":site"</a> angezeigt.',
];
