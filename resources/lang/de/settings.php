<?php

return [
    'head.1'            => 'Einstellungen',
    'head.2'            => 'Hier können Sie Ihr MetaGer anpassen: Nach Anklicken Ihrer gewünschten Einstellungen müssen Sie <a href="#unten">unten auf dieser Seite</a> wählen, ob Sie die Einstellungen dauerhaft speichern, oder nur einmalig setzen wollen.',

    'allgemein.1'       => 'Allgemein',

    'quotes.label'      => 'Sprüche',
    'quotes.on'         => 'anzeigen',
    'quotes.off'        => 'nicht anzeigen',

    'maps.label'        => 'Maps',
    'maps.on'           => 'anzeigen',
    'maps.off'          => 'nicht anzeigen',

    'tab.label'         => 'Ergebnisse öffnen:',
    'tab.new'           => 'In einem neuen Tab',
    'tab.same'          => 'Im gleichen Tab',

    'language.label'    => 'Sprache auswählen:',
    'language.all'      => 'Alle Sprachen',
    'language.de'       => 'Deutsch',
    'language.en'       => 'Englisch',

    'suchmaschinen.1'   => 'Suchmaschinen',
    'suchmaschinen.2'   => '(alle abwählen)',
    'suchmaschinen.3'   => '(alle an-/abwählen)',

    'speichern.1'       => 'Startseite für einmalige Nutzung generieren',
    'speichern.2'       => 'Einstellungen dauerhaft speichern',
    'speichern.3'       => 'Plugin mit diesen Einstellungen generieren',
    'speichern.4'       => 'Einstellungen zurücksetzen',

    'request'           => 'Abfragemethode',

    'autocomplete'      => 'Auto-Vervollständigung (Sucheingabe)',
    'autocomplete.on'   => 'ein',
    'autocomplete.off'  => 'aus',

    "foki.web"          => "Web",
    "foki.andere"       => "Andere",
    "foki.produktsuche" => "Produktsuche",
    "foki.wissenschaft" => "Wissenschaft",
    "foki.nachrichten"  => "News/Politik",
    "foki.bilder"       => "Bilder",
];
