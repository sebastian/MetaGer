<?php

return [
    "head.1" => "About us",
    "head.2" => "What are the advantages of using MetaGer?",
    "list.1" => "Privacy is quite natural for us: Completely built in and automatically applied to every search. <a href=\"/en/datenschutz/\">More about privacy...</a>",
    "list.2" => "We do not work profit-oriented, we are a <a href=\"/en/spende/\">charitable association</a>: Our goal is not to enrich ourselves with your clicks or even your data.",
    "list.3" => "<a href=\"https://en.wikipedia.org/wiki/MetaGer\" target=\"_blank\" rel=\"noopener\">MetaGer</a> is primarily a <a href=\"https://de.wikipedia.org/wiki/Metasuchmaschine\" target=\"_blank\" rel=\"noopener\">meta search engine</a>: We query up to 50 search engines. Thus we can offer real variety in results.",
    "list.4" => "For our results, we do not prefer results <a href=\"https://en.wikipedia.org/wiki/Filter_bubble\" target=\"_blank\" rel=\"noopener\">that are clicked more often</a>: This also gives you variety, instead of mainstream.",
    "list.5" => "MetaGer is online since about 20 years: Our experience is your advantage - we know what we do.",
    "list.6" => "We only use green electricity for our servers.",
    "list.7" => "But we are not perfect as well: If you encounter something weird: Please <a href=\"/en/kontakt/\" target=\"_blank\" rel=\"noopener\">contact us</a>! We take your hints seriously: <em>You</em> are most important for us.",
];
