<?php

return [
    "quicktips.wikipedia.adress" => "from <a href=\"https://en.wikipedia.org\" target=\"_blank\" rel=\"noopener\"> Wikipedia, The Free Encyclopedia",
    "quicktips.dictcc.adress"    => 'from <a href="https://www.dict.cc/" target="_blank" rel="noopener">dict.cc</a>',
    "quicktips.tips.title"       => "Did you know?",
];
