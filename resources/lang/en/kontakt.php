<?php

return [
    'headline.1' => 'Contact',
    'headline.2' => 'Forum',
    'headline.3' => 'If you have a technical question, ask it at our <a href="http://forum.suma-ev.de/" target="_blank" rel="noopener">board</a> first, please',
    'headline.4' => 'By this way others can benefit from an answer.',

    'form.1'     => 'Secure Contact Form',
    'form.2'     => 'Via this form you can contact us by encrypted <a href="mailto:office@suma-ev.de">email</a>.',

    'form.3'     => 'Please note that due to a lot of requests we are personnel-wise not able to answer everything promptly.',
    'form.4'     => 'If you do not enter an email-adress, we can not answer you.',
    'form.5'     => 'Your e-mail-adress (optional)',
    'form.6'     => 'Your message',
    'form.7'     => '<strong>Before dispatch, your messgae is encrypted with <a href="http://openpgpjs.org/.">OpenPGP.js</a>. We use Javascript for this.</strong> If you have Javascript disabled, your message will be send unencrypted.',
    'form.8'     => 'Encrypt and send',

    'mail.1'     => 'By Email',
    'mail.2'     => 'You can also email us directly at: <a href="mailto:office@suma-ev.de">office@suma-ev.de</a>',
    'mail.3'     => 'If you want to encrypt your email you can use our following public OpenPGP-Key:',
    'letter.1'   => 'By Letter Mail',
    'letter.2'   => 'We prefer digital contact. However, if you consider it neccessary to contact us postally, you can mail us at:',
    'letter.3'   => 'SUMA-EV
Röselerstr. 3
30159 Hannover
Germany',
];
