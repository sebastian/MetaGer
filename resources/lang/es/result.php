<?php

return [
    "options.1" => "Nueva búsqueda en este dominio",
    "options.2" => "ocultar :host",
    "options.3" => "ocultar *.:domain",
    "options.4" => "Tienda asociada",
    "options.5" => "abrir anónimo",
    "proxytext" => "Abrirá el link anonimizado.",
];
