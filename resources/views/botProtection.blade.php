<!DOCTYPE html>
<html>
	<head>
		<title>Access Denied</title>
		<link href="/favicon.ico" rel="icon" type="image/x-icon" />
		<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<meta content="width=device-width, initial-scale=1.0, user-scalable=no" name="viewport" />
		<meta content="{{ getmypid() }}" name="p" />
		<meta content="#wknekjnbweignipwep==" name="q" />
		<meta content="{{ $hash }}" name="pq" />
		<meta content="{{ $r }}" name="pqr" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link type="text/css" rel="stylesheet" href="/css/themes/{{ app('request')->input('theme', 'default') }}.css" />
	</head>
	<body>
		<h1 class="hidden">Your Access to this site has been denied. Please contact <a href="mailto:office@suma-ev.de">office@suma-ev.de</a> if this is not correct</h1>
		<script type="text/javascript" src="/js/all.js"></script>
	</body>
</html>
